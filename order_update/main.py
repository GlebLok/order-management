import aiohttp
import os
import json
import base64
from dotenv import load_dotenv
import asyncio
from sqlalchemy import create_engine, MetaData, Table


engine = create_engine('postgresql://postgres:postgres@postgres:5432/orders')

metadata = MetaData()

metadata.bind = engine

orders_table = Table('api_order', metadata, autoload_with=engine)

def encode_to_base64(data: str):
    data_bytes = data.encode('utf-8')
    encoded_data = base64.b64encode(data_bytes)
    encoded_to_str = encoded_data.decode('utf-8')

    return encoded_to_str


def update_order_in_database(order_data):
    order_id = order_data.get('id')
    
    with engine.connect() as connection:
        update_order = orders_table.update().where(orders_table.c.id == order_id).values(**order_data)
        connection.execute(update_order)
        return True
    
    
async def fetch_data_and_update_db():
    load_dotenv()
    url = "https://broker-api.sandbox.alpaca.markets/v2beta1/events/trades?since=2024-01-30"
    
    keys = f'{os.environ.get("API_KEY")}:{os.environ.get("API_SECRET")}'
    headers = {
        "accept": "text/event-stream",
        "authorization": f"Basic {encode_to_base64(keys)}"
    }
    
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(url, headers=headers) as response:
                async for line in response.content:
                    line_dec = line.decode()
                    if line_dec != '\n' and line_dec != ': welcome to the Alpaca events\n' and line_dec != ': heartbeat\n':
                        json_line = json.loads(line_dec[len('data: '):])
                        order = json_line.get('order')
                        if "cancel_requested_at" in order:
                            order.pop("cancel_requested_at")
                    
                        updated = update_order_in_database(order)
                        if updated:
                            print('Order updated success')
    
    except TimeoutError:
        await asyncio.sleep(5)
        
        await fetch_data_and_update_db()

async def main():
    try:
        await fetch_data_and_update_db()
    except TimeoutError:
        print("TimeoutError occurred. Restarting the function...")
        await asyncio.sleep(5)
        await fetch_data_and_update_db()


if __name__ == "__main__":
    asyncio.run(main())
