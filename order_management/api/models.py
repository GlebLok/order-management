from django.db import models


class Order(models.Model):
    SIDE_CHOICES = [
        ('buy', 'buy'), ('sell', 'sell')     
    ]

    TYPE_CHOICES = [
        ('market', 'market'), ('limit', 'limit'), ('stop', 'stop'),
        ('stop_limit', 'stop_limit'), ('trailing_stop', 'trailing_stop')
    ]

    TIME_IN_FORCE_CHOICES = [
        ('day', 'day'), ('gtc', 'gtc'), ('opg', 'opg'),
        ('cls', 'cls'), ('ios', 'ios'), ('fok', 'fok')
    ]

    id = models.UUIDField(primary_key=True)
    client_order_id = models.CharField(max_length=48, unique=True, null=True, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    submitted_at = models.DateTimeField()
    filled_at = models.DateTimeField(null=True, blank=True)
    expired_at = models.DateTimeField(null=True, blank=True)
    canceled_at = models.DateTimeField(null=True, blank=True)
    failed_at = models.DateTimeField(null=True, blank=True)
    replaced_at = models.DateTimeField(null=True, blank=True)
    replaced_by = models.CharField(null=True, blank=True)
    replaces = models.CharField(null=True, blank=True)
    asset_id = models.CharField()
    symbol = models.CharField(max_length=10)
    asset_class = models.CharField(max_length=20)
    notional = models.DecimalField(max_digits=20, decimal_places=10, null=True, blank=True)
    qty = models.DecimalField(max_digits=20, decimal_places=10, null=True, blank=True)
    filled_qty = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    filled_avg_price = models.DecimalField(max_digits=20, decimal_places=10, null=True, blank=True)
    order_class = models.CharField(max_length=50, blank=True, default='')
    order_type = models.CharField(max_length=50)
    type = models.CharField(max_length=50, choices=TYPE_CHOICES)
    side = models.CharField(max_length=40, choices=SIDE_CHOICES)
    time_in_force = models.CharField(max_length=50, choices=TIME_IN_FORCE_CHOICES)
    limit_price = models.DecimalField(max_digits=20, decimal_places=4, null=True, blank=True)
    stop_price = models.DecimalField(max_digits=20, decimal_places=4, null=True, blank=True)
    status = models.CharField(max_length=50)
    extended_hours = models.BooleanField(default=False)
    legs = models.JSONField(null=True, blank=True)
    trail_percent = models.DecimalField(max_digits=10, decimal_places=4, null=True, blank=True)
    trail_price = models.DecimalField(max_digits=20, decimal_places=4, null=True, blank=True)
    hwm = models.DecimalField(max_digits=20, decimal_places=4, null=True, blank=True)
    commission = models.DecimalField(max_digits=20, decimal_places=4, default=0)
    subtag = models.CharField(max_length=50, null=True, blank=True)
    source = models.CharField(max_length=50, null=True, blank=True)
