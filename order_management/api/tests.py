from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework.exceptions import ErrorDetail
import uuid




class OrderCreateApiViewTestCase(TestCase):
    def test_create_order_success_base(self):
        url = reverse('order-create')
        data = {
            "side": "sell",
            "type": "market",
            "time_in_force": "day",
            "symbol": "AAPL",
            "qty": "4.124"
        }
        
        client = APIClient()
        response = client.post(url, data, format='json')
        
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


    def test_create_order_failure_side(self):
        url = reverse('order-create')
        data = {  
            "type": "market",
            "time_in_force": "day",
            "symbol": "AAPL",
            "qty": "4.124"
        }
        
        client = APIClient()
        response = client.post(url, data, format='json')
        
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('side', response.data)
        self.assertEqual(response.data['side'], [ErrorDetail(string='This field is required.', code='required')])
    

    def test_create_order_success_with_cl_order_id(self):
        url = reverse('order-create')
        data = {
            "side": "buy",
            "type": "market",
            "time_in_force": "day",
            "symbol": "AAPL",
            "qty": "4.124",
            "client_order_id": uuid.uuid4()
        }
        
        client = APIClient()
        response = client.post(url, data, format='json')
        
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    
    def test_create_order_failure_with_cl_order_id(self):
        url = reverse('order-create')
        data = {
            "side": "sell",
            "type": "market",
            "time_in_force": "day",
            "symbol": "AAPL",
            "qty": "4.124",
            "client_order_id": "aaaaaaa"
        }
        
        client = APIClient()
        response = client.post(url, data, format='json')
    
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, 'Api Error')
    

    def test_create_order_failure_with_extended_hours(self):
        url = reverse('order-create')
        data = {
            "side": "buy",
            "type": "market",
            "time_in_force": "day",
            "symbol": "AAPL",
            "qty": "4.124",
            "extended_hours": True
        }
        
        client = APIClient()
        response = client.post(url, data, format='json')
    
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, 'Api Error')

    
    def test_create_order_success_with_limit(self):
        url = reverse('order-create')
        data = {
            "side": "sell",
            "type": "limit",
            "time_in_force": "day",
            "symbol": "AAPL",
            "qty": "2",
            "limit_price": "3"
        }
        
        client = APIClient()
        response = client.post(url, data, format='json')
    
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        
    
    def test_create_order_success_with_notional(self):
        url = reverse('order-create')
        data = {
            "side": "sell",
            "type": "market",
            "time_in_force": "day",
            "symbol": "AAPL",
            "notional": "2",
        }
        
        client = APIClient()
        response = client.post(url, data, format='json')
    
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    

    def test_create_order_failure_with_qty(self):
        url = reverse('order-create')
        data = {
            "side": "buy",
            "type": "market",
            "time_in_force": "day",
            "symbol": "AAPL",
            "qty": "4.124",
            "notional": "2"
        }
        
        client = APIClient()
        response = client.post(url, data, format='json')
    
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('non_field_errors', response.data)
        self.assertEqual(response.data['non_field_errors'], 
                         [ErrorDetail(string='Only one of qty or notional is accepted', code='invalid')])
    

    def test_create_order_failure_with_limit(self):
        url = reverse('order-create')
        data = {
            "side": "buy",
            "type": "limit",
            "time_in_force": "day",
            "symbol": "AAPL",
            "qty": "4.124",
        }
        
        client = APIClient()
        response = client.post(url, data, format='json')
    
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('non_field_errors', response.data)
        self.assertEqual(response.data['non_field_errors'], 
                         [ErrorDetail(string='Limit orders require a limit price', code='invalid')])
    

    def test_create_order_failure_with_stop(self):
        url = reverse('order-create')
        data = {
            "side": "buy",
            "type": "stop",
            "time_in_force": "day",
            "symbol": "AAPL",
            "qty": "4.124",
        }
        
        client = APIClient()
        response = client.post(url, data, format='json')
    
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('non_field_errors', response.data)
        self.assertEqual(response.data['non_field_errors'], 
                         [ErrorDetail(string='Stop orders require a stop price', code='invalid')])
    

    def test_create_order_failure_with_stop_limit(self):
        url = reverse('order-create')
        data = {
            "side": "buy",
            "type": "stop_limit",
            "time_in_force": "day",
            "symbol": "AAPL",
            "qty": "4.124",
        }
        
        client = APIClient()
        response = client.post(url, data, format='json')
    
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('non_field_errors', response.data)
        self.assertEqual(response.data['non_field_errors'], 
                         [ErrorDetail(string='Stop limit orders require both stop and limit price', code='invalid')])
    

    def test_create_order_failure_with_trailing_stop(self):
        url = reverse('order-create')
        data = {
            "side": "buy",
            "type": "trailing_stop",
            "time_in_force": "day",
            "symbol": "AAPL",
            "qty": "4.124",
        }
        
        client = APIClient()
        response = client.post(url, data, format='json')
    
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('non_field_errors', response.data)
        self.assertEqual(response.data['non_field_errors'], 
                         [ErrorDetail(
                             string='Trailing stop orders must specify one of trail_price or trail_percent', 
                             code='invalid')])
        
    
    def test_create_order_failure_with_trl_cent_price(self):
        url = reverse('order-create')
        data = {
            "side": "buy",
            "type": "trailing_stop",
            "time_in_force": "day",
            "symbol": "AAPL",
            "qty": "4.124",
            "trail_price": "3",
            "trail_percent": "4"
        }
        
        client = APIClient()
        response = client.post(url, data, format='json')
    
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('non_field_errors', response.data)
        self.assertEqual(response.data['non_field_errors'], 
                         [ErrorDetail(
                             string='Trailing stop orders cannot specify both trail_price and trail_percent', 
                             code='invalid')])



class OrderCancelAPIViewTestCase(TestCase):
    def setUp(self):
        url = reverse('order-create')
        data = {
            "side": "sell",
            "type": "market",
            "time_in_force": "day",
            "symbol": "AAPL",
            "qty": "4.124"
        }
        
        client = APIClient()
        response = client.post(url, data, format='json')
        self.order = response.data

    def test_cancel_order_success(self):
        url = reverse('order-cancel', kwargs={'order_id': self.order.get('id')})

        client = APIClient()
        response = client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    