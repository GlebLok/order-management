from rest_framework import serializers
from .models import Order

class OrderListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'

        
class OrderCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields =[
            'symbol', 'qty', 'notional', 'side', 
            'type', 'time_in_force', 'limit_price', 'stop_price', 
            'trail_price', 'trail_percent', 'extended_hours', 
            'client_order_id', 'order_class', 'commission', 
            'subtag', 'source'
        ]

    def validate(self, data):
        qty = data.get('qty')
        notional = data.get('notional')
        type = data.get('type')
        limit_price = data.get('limit_price')
        stop_price = data.get('stop_price')
        trail_price = data.get('trail_price')
        trail_percent = data.get('trail_percent')
        time_in_force = data.get('time_in_force')
        extended_hours = data.get('extended_hours')
            
        if qty and notional:
            raise serializers.ValidationError('Only one of qty or notional is accepted')
            
        if not qty and not notional:
            raise serializers.ValidationError('Qty or notional is required')
            
        if type == 'limit' and not limit_price:
            raise serializers.ValidationError('Limit orders require a limit price')
            
        if type == 'stop' and not stop_price:
            raise serializers.ValidationError('Stop orders require a stop price')
            
        if type == 'stop_limit' and not limit_price and not stop_price:
            raise serializers.ValidationError('Stop limit orders require both stop and limit price')
            
        if type == 'trailing_stop' and not trail_percent and not trail_price:
            raise serializers.ValidationError('Trailing stop orders must specify one of trail_price or trail_percent')
            
        if trail_percent and trail_price: 
            raise serializers.ValidationError('Trailing stop orders cannot specify both trail_price and trail_percent')
            
        if extended_hours and type != 'limit' and time_in_force != 'day':
            raise serializers.ValidationError('Extended hours order must be DAY limit orders')
        
        return data
            