# Generated by Django 5.0.1 on 2024-01-30 22:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0008_alter_order_client_order_id_alter_order_order_class_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='filled_qty',
            field=models.DecimalField(decimal_places=19, default=0, max_digits=20),
        ),
    ]
