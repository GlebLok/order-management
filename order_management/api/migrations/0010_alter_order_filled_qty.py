# Generated by Django 5.0.1 on 2024-01-30 22:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0009_alter_order_filled_qty'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='filled_qty',
            field=models.DecimalField(decimal_places=10, default=0, max_digits=20),
        ),
    ]
