from django.shortcuts import render
from rest_framework import generics, status
from rest_framework.response import Response
from .models import Order
from .serializers import OrderListSerializer, OrderCreateSerializer
import os
import requests
import base64
from dotenv import load_dotenv
import json
from datetime import datetime
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema




def encode_to_base64(data: str):
    data_bytes = data.encode('utf-8')
    encoded_data = base64.b64encode(data_bytes)
    encoded_to_str = encoded_data.decode('utf-8')

    return encoded_to_str


class OrderListApiView(generics.ListAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderListSerializer

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter('status', openapi.IN_QUERY, description="Status of orders", type=openapi.TYPE_STRING),
            openapi.Parameter('limit', openapi.IN_QUERY, description="Limit number of orders", type=openapi.TYPE_INTEGER),
            openapi.Parameter('after', openapi.IN_QUERY, description="Date after which orders were placed", type=openapi.TYPE_STRING, format='date-time'),
            openapi.Parameter('until', openapi.IN_QUERY, description="Date until which orders were placed", type=openapi.TYPE_STRING, format='date-time'),
            openapi.Parameter('direction', openapi.IN_QUERY, description="Direction of sorting", type=openapi.TYPE_STRING),
            openapi.Parameter('symbols', openapi.IN_QUERY, description="Symbols of orders", type=openapi.TYPE_STRING),
            openapi.Parameter('qty_above', openapi.IN_QUERY, description="Quantity above", type=openapi.TYPE_INTEGER),
            openapi.Parameter('qty_below', openapi.IN_QUERY, description="Quantity below", type=openapi.TYPE_INTEGER),
            openapi.Parameter('subtag', openapi.IN_QUERY, description="Subtag", type=openapi.TYPE_STRING),
        ]
    )

    def get(self, request, *args, **kwargs):
        query_params = request.query_params
        data = self.get_data(query_params)
        
        if data:
            for item in data:
            
                try:
                    Order.objects.get(id=item['id'])
            
                except Order.DoesNotExist:
                    serializer = OrderListSerializer(data=item)
            
                    if serializer.is_valid():
                        serializer.save()
            
                    else:
                        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        
        return super().get(request, *args, **kwargs)     

    
    def get_data(self, query: dict):
        load_dotenv()
        
        url = f"https://broker-api.sandbox.alpaca.markets/v1/trading/accounts/{os.environ.get('Broker_id')}/orders"
        
        if query:
            query_str = "&".join([f"{key}={value}" for key, value in query.items()])
            url = f'{url}?{query_str}'

        keys = f'{os.environ.get("API_KEY")}:{os.environ.get("API_SECRET")}'

        headers = {
            "accept": "application/json",
            "authorization": f"Basic {encode_to_base64(keys)}"
        }

        response = requests.get(url, headers=headers)
        
        if response.status_code == 200:
            return json.loads(response.text) 
        
        else:
            return False


    def get_queryset(self):
        queryset = super().get_queryset()
        query_params = self.request.query_params
        
        limit_value = None

        for key, value in query_params.items():
            
            if key == 'status':
                
                if value == 'open':
                    queryset = queryset.filter(status__in=['accepted', 'new'])
                
                elif value == 'closed':
                    queryset = queryset.filter(status__in=["canceled", "filled"])

            elif key == 'limit':
                limit_value = value

            elif key == 'after':
                queryset = queryset.filter(submitted_at__gt=value)
            
            elif key == 'until':
                queryset = queryset.filter(submitted_at__lt=value)
            
            elif key == 'direction':
                queryset = queryset.order_by('-submitted_at' if value == 'desc' else 'submitted_at')
            
            elif key == 'symbols':
                queryset = queryset.filter(symbol__in=value.split(','))
            
            elif key == 'qty_above':
                queryset = queryset.filter(quantity__gt=value)
            
            elif key == 'qty_below':
                queryset = queryset.filter(quantity__lt=value)
            
            elif key == 'subtag':
                 queryset = queryset.filter(subtag=value) 
        
        if limit_value:
            queryset = queryset[:int(limit_value)]
        
        return queryset



class OrderCreateApiView(generics.CreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderCreateSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        
        if serializer.is_valid():
            data = self.send_data(serializer.data) 

            if data is not False:
                Order.objects.create(**data)
            
            else: 
                return Response('Api Error', status=status.HTTP_400_BAD_REQUEST)
            
            return Response(data, status=status.HTTP_201_CREATED)
  
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    
    def send_data(self, payload_data):
        load_dotenv()
        
        url = f"https://broker-api.sandbox.alpaca.markets/v1/trading/accounts/{os.environ.get('Broker_id')}/orders"

        payload = payload_data
        
        keys = f'{os.environ.get("API_KEY")}:{os.environ.get("API_SECRET")}'
        headers = {
            "accept": "application/json",
            "content-type": "application/json",
            "authorization": f"Basic {encode_to_base64(keys)}"
        }

        response = requests.post(url, json=payload, headers=headers)

        if response.status_code == 200:
            return json.loads(response.text)

        else:
            return False


class OrderCancelAPIView(generics.DestroyAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderListSerializer
    lookup_url_kwarg = 'order_id'

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()

        if instance.status == 'canceled':
            return Response(f'Order {instance.id} is already canceled', status=status.HTTP_400_BAD_REQUEST)
        
        data = self.canceled_data()
        
        if data:
            time_object = datetime.now()
            time_string = time_object.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
            instance.status = 'canceled'
            instance.canceled_at = time_string 
            instance.save()
            
            return Response(f'Order {instance.id} canceled successfully', status=status.HTTP_204_NO_CONTENT)
        
        return Response(f'Order with id {instance.id} does not exist', status=status.HTTP_400_BAD_REQUEST)
    
    
    def canceled_data(self):
        load_dotenv()
        
        order_id = self.kwargs.get('order_id')
        url = f"https://broker-api.sandbox.alpaca.markets/v1/trading/accounts/{os.environ.get('Broker_id')}/orders/{order_id}"
        
        keys = f'{os.environ.get("API_KEY")}:{os.environ.get("API_SECRET")}'
        headers = {
            "accept": "application/json",
            "authorization": f"Basic {encode_to_base64(keys)}"
        }

        response = requests.delete(url, headers=headers)

        if response.status_code == 204:
            return True
        return False